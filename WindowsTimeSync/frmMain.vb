﻿' Windows Time Sync -- Program to synchronize the windows clock once a day.
' Copyright 2021 Eric Cavaliere
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Public Class formMain
    Dim LastSync As Date = Nothing
    Dim RunAtTime As New DateTime(DateTime.Now().Year, DateTime.Now().Month, DateTime.Now().Day, 2, 1, 0) ' 2:01 am

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Hide()
        SyncTime()
        Timer1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        ' If we're on or after the target hour / minute and a sync didn't happen today, then run a sync
        If DateTime.Now().Hour = RunAtTime.Hour And DateTime.Now().Minute >= RunAtTime.Minute Then
            If LastSync.Day <> DateTime.Now().Day Then
                SyncTime()
            End If
        End If
    End Sub

    Private Sub SyncTime()
        Try
            Dim w32time As New Process
            w32time.StartInfo.Verb = "runas" ' Run as admin
            w32time.StartInfo.UseShellExecute = True
            w32time.StartInfo.FileName = "net"
            w32time.StartInfo.Arguments = "start w32time"
            w32time.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            w32time.Start()
            w32time.WaitForExit()

            Dim w32tm As New Process
            w32tm.StartInfo.Verb = "runas" ' Run as admin
            w32tm.StartInfo.UseShellExecute = True
            w32tm.StartInfo.FileName = "w32tm"
            w32tm.StartInfo.Arguments = "/resync"
            w32tm.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            w32tm.Start()
            w32tm.WaitForExit()

            LastSync = DateTime.Now()

        Catch ex As Exception

        End Try
    End Sub
End Class
