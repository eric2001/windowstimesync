# Windows Time Sync

## Description
This program will synchronize the windows clock with the configured time server when it initially starts and will then run in the background and resync at 2 am every day.

**License:** [GPL v.3](http://www.gnu.org/copyleft/gpl.html)

## Instructions
Run the program.  It will synchronize and then continue to run in the background to re-synchronize the clock every morning.

## History
**Version 1.0.0:**
> - Initial Release
> - Released on 20 March 2021
>
> Download: [Version 1.0.0 Binary](/uploads/68d9633bb64b0247bbcdd62ad8587ec2/WindowsTimeSync100Binary.zip) | [Version 1.0.0 Source](/uploads/7fe056163e11cfae2aea2e6d421285da/WindowsTimeSync100Source.zip)
